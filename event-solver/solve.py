import requests, json, datetime
from typing import List, Dict, Tuple, Any, Union

# TODO: Use TypedDict in python3.8
Partner = Dict[str, Union[str, List[str]]]  
EventInfo = Dict[str, List[Dict[str, Any]]]


def get_start_dates(available_dates: List[str]) -> List[str]:
    """Gets the event start dates from a partners available dates.
    Event start dates require two consecutive available dates.

    :param available_dates: A list of the available date strings in ISO8601 format.
    :type available_dates: A list of strings.
    :return: Returns a list of valid start date strings.
    :rtype: A list of strings."""

    # Initialize start dates
    start_dates = []

    # Check each date string (except the last)
    for date_string in available_dates[:-1]:
        date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
        next_date = date + datetime.timedelta(days=1)
        next_date_string = next_date.strftime("%Y-%m-%d")
        if next_date_string in available_dates:
            start_dates.append(date_string)

    # Return start dates
    return start_dates


def get_possible_dates(partners: List[Partner]) -> Tuple[Dict[str, Any], set]:
    """Gets all possible event dates for the partners.

    :param partners: A list of each partner object.
    :type partners: A list of dicts.
    :return: Returns a tuple with a dict of all possible dates and a set of country names.
    :rtype: A tuple with a dict and a set."""

    # Initialize parameters
    possible_dates: Dict[Any, Any] = {}
    country_names = set()

    # Iterate over partners
    for partner in partners:

        # Get partner parameters
        email = partner.get("email")
        country = partner.get("country")
        country_names.add(country)
        available_dates: List[str] = partner.get("availableDates", [])  # type: ignore
        start_dates = get_start_dates(available_dates)

        # Check for new country
        if country not in possible_dates.keys():
            possible_dates[country] = {}

        # Verify partner has can attend event
        if len(start_dates) < 1:
            continue

        # Add partner to possible event dates
        for start_date in start_dates:

            # Verify start date entry exists for the country
            if start_date not in possible_dates[country]:
                possible_dates[country][start_date] = []

            # Add partner to possible event date
            possible_dates[country][start_date].append(email)

    # Successfully got possible dates
    return possible_dates, country_names


def get_best_date(
    possible_dates: Dict[str, Dict[str, List[str]]], countries: set
) -> Dict[Any, Any]:
    """Gets the best event date from all possible dates for each country.

    :param possible_dates: A dict of all possible dates for each country.
    :type possible_dates: A dict.
    :return: Returns a dict of the best event date for each country.
    :rtype: A dict."""

    # Initialize best date
    best_date: Dict[str, Dict[str, Any]] = {}
    for country in countries:
        best_date[country] = {
            "attendees": [],
            "attendeeCount": 0,
            "startDate": None,
        }

    # Compute best date
    for country, country_dates in possible_dates.items():
        for start_date, attendees in country_dates.items():

            # Verify event date is the date with the most attendees
            attendee_count = len(attendees)
            if attendee_count > best_date[country]["attendeeCount"]:  # type: ignore
                best_date[country] = {
                    "attendees": attendees,
                    "attendeeCount": attendee_count,
                    "startDate": start_date,
                }

            # Verify event date with most attendees is the soonest date
            elif (
                attendee_count == best_date[country]["attendeeCount"]
                and start_date < best_date[country]["startDate"]
            ):
                best_date[country] = {
                    "attendees": attendees,
                    "attendeeCount": attendee_count,
                    "startDate": start_date,
                }

    # Return best date
    return best_date


def get_event_info(partners: List[Partner]) -> EventInfo:
    """Gets event information provided a list of patners in various countries
    with varrying availabilities. The event date is chosen for each country
    such that the highest number of partners can attend on the soonest date.

    :param partners: A list of each partner object.
    :type partners: A list of dicts.
    :return: Returns a dict of the event date and attendees for each country.
    :rtype: A dict."""

    # Get event date
    possible_dates, country_names = get_possible_dates(partners)
    best_date = get_best_date(possible_dates, country_names)

    # Format event info
    event_info: EventInfo = {"countries": []}
    for country, country_data in best_date.items():
        event_info["countries"].append(
            {
                "name": country,
                "attendeeCount": country_data["attendeeCount"],
                "attendees": country_data["attendees"],
                "startDate": country_data["startDate"],
            }
        )

    # Return event info
    return event_info


if __name__ == "__main__":
    """Gets a list of partners and their availabilities from an endpoint
    then figures out the best date to hold an event in each country then posts
    the event date and attendees to another endpoint. The event date is chosen
    such that the highest number of partners can attend on the soonest date."""

    # Get partners from file
    with open("partners.json", "r") as json_file:
        data = json.load(json_file)
    partners = data.get("partners", [])

    # Get event info
    event = get_event_info(partners)

    # Write event info to file
    with open("event.json", "w") as json_file:
        json.dump(event, json_file)


# Requests Reference
# response = requests.get(partners_url)
# data = response.json()
# response = requests.post(event_url, json=event)
# print(f'Submission status: {response.status_code} ({response.reason})')
