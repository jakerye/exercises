# Event Solver
Reads in a json file containing a list of partners who reside in various countries and have various availabilites.

This creates a solver to figure out the best time to hold a conference in each country. Conferences must span 2 consequitive days and should include as many people as possible and then as soon as possible.

It then writes the event information back to a json file.
