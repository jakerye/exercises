const mysql = require('mysql');

const PASSWORD = 'mypassword'

const pool = mysql.createPool({
  connectionLimit: 5,
  host: 'localhost',
  user: 'root',
  password: PASSWORD,
  database: 'mydatabase',
});

function getEntriesBeforeDate(tableName, date, callback) {
  const timestamp = date.toISOString().replace('T', ' ')
    .replace('Z', '').split('.')[0];
  const sql = `SELECT * FROM ${tableName} WHERE time < '${timestamp}'`;

  pool.getConnection((error, connection) => {
    if (error) {
      console.error('Unable to get connection: ', error);
      return callback();
    };
    connection.query(sql, (error, rows) => {
      connection.release();
      if (error) {
        console.error('Unable to query table:', error)
        return callback();
      }
      return callback(rows);
    });
    connection.on('error', (error) => {
      console.log('Error in database connection:', error)
      return callback();
    });
  });
}

const date = new Date();
getEntriesBeforeDate('mytable', date, (rows) => console.log('Rows:', rows));
