const mysql = require('mysql');

const PASSWORD = 'mypassword';

const pool = mysql.createPool({
    connectionLimit: 5,
    host: 'localhost',
    user: 'root',
    password: PASSWORD,
    database: 'mydatabase',
});


function createDatabase(databaseName) {
    const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "mypassword",
    });

    connection.connect((error) => {
        if (error) throw error;
        console.log("Connected!");
        const databaseSql = `CREATE DATABASE ${databaseName}`
        connection.query(databaseSql, (error, result) => {
            if (error) throw error;
            console.log("Database created");
        });
    });
}

function createTable(databaseName, tableSql) {
    const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "mypassword",
        database: databaseName,
    });

    connection.connect((error) => {
        if (error) throw error;
        console.log("Connected!");
        connection.query(tableSql, (error, result) => {
            if (error) throw error;
            console.log("Table created");
        });
    });
}


function createEntry(databaseName, tableName, entryId, entryName) {
    const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "mypassword",
        database: databaseName,
    });

    connection.connect((error) => {
        if (error) throw error;
        console.log("Connected!");
        const date = new Date(Date.now());
        const timestamp = date.toISOString().replace('T', ' ').replace('Z', '').split('.')[0];
        console.log('timestamp:', timestamp)
        const sql = `INSERT INTO ${tableName} (id, time, name) VALUES ('${entryId}', '${timestamp}', '${entryName}')`;
        connection.query(sql, (error, result) => {
            if (error) throw error;
            console.log("1 record inserted");
        });
    });
}

function getEntries(databaseName, tableName) {
    const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "mypassword",
        database: databaseName,
    });

    connection.connect((error) => {
        if (error) throw error;
        console.log("Connected!");
        const sql = `SELECT * FROM ${tableName}`;
        connection.query(sql, (error, rows) => {
            if (error) throw error;
            console.log('rows:', rows);
        });
    });
}

function getEntriesAfterDate(databaseName, tableName, timestamp) {
    const connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "mypassword",
        database: databaseName,
    });

    connection.connect((error) => {
        if (error) throw error;
        console.log("Connected!");
        const sql = `SELECT * FROM ${tableName} WHERE time > '${timestamp}'`;
        connection.query(sql, (error, rows) => {
            if (error) throw error;
            console.log('rows:', rows);
        });
    });
}


function getEntriesBeforeDate(tableName, date, callback) {
    const timestamp = date.toISOString().replace('T', ' ')
        .replace('Z', '').split('.')[0];
    const sql = `SELECT * FROM ${tableName} WHERE time < '${timestamp}'`;

    pool.getConnection((error, connection) => {
        if (error) {
            console.error('Unable to get connection: ', error);
            return callback();
        };
        connection.query(sql, (error, rows) => {
            connection.release();
            if (error) {
                console.error('Unable to query table:', error)
                return callback();
            }
            return callback(rows);
        });
        connection.on('error', (error) => {
            console.log('Error in database connection:', error)
            return callback();
        });
    });
}


function deleteMostRecentEntries(tableName, numEntries, callback) {
    const sql = `DELETE FROM ${tableName} ORDER BY time DESC LIMIT ${numEntries}`;

    const sql = `SELECT * FROM ${tableName} ORDER BY time DESC LIMIT ${numEntries}`;


    pool.getConnection((error, connection) => {
        if (error) {
            console.error('Unable to get connection: ', error);
            return callback();
        };
        connection.query(sql, (error, rows) => {
            connection.release();
            if (error) {
                console.error('Unable to query table:', error)
                return callback();
            }
            return callback(rows);
        });
        connection.on('error', (error) => {
            console.log('Error in database connection:', error)
            return callback();
        });
    });
}


const databaseName = 'mydatabase';
const tableName = 'mytable';

// createDatabase(databaseName);
// const tableSql = `CREATE TABLE ${tableName} (id VARCHAR(50), time TIMESTAMP, name TEXT)`;
// createTable(databaseName, tableSql);
// createEntry(databaseName, tableName, '3', 'Hannah');
// getEntries(databaseName, tableName);

// const date = new Date();
// date.setDate(date.getDate() - 5);
// const timestamp = date.toISOString().replace('T', ' ').replace('Z', '').split('.')[0];
// getEntriesAfterDate(databaseName, tableName, timestamp);

// const date = new Date();
// getEntriesBeforeDate('mytable', date, (rows) => console.log('Rows:', rows));

deleteMostRecentEntries('mytable', 3, (rows) => console.log('Rows:', rows));