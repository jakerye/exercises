# Code Review: SQL Connection Pool
See the included initialCode.js and reviewedCode.js files.

## Review
This code is promising but needs a few tweaks. It seems like this function gets all database entries before a particular date so the function name should reflect that. A more suitable name would be something like ‘getEntriesBeforeDate’. Also, having specific variable names helps other developers understand code quicker so it’d be a good idea to rename value to `date` since that is what the value will always represent in the function. Being able to pass a callback to the function is another great idea to promote flexible development, just make sure the callback function actually gets called.

Using a connection pool is another great idea as it is a nice way to increase performance by recycling a pool of connections instead of creating a new connection upon each request. Since creating a new connection takes time, it will make the requests faster, but only if the connections aren’t being recreated upon each request. To get this performance benefit, it is important to create the connection pool outside of the function scope so it does not get recreated on each call.

Another thing to be careful with is your sql command syntax as comparison parameters need to wrapped in single quotes. I’d recommend using template strings to help format the sql command.

Also be sure to connect and query the database with the proper methods. Check out this tutorial, specifically the section titled ‘Pool connection in MySQL’, on how to interact with a mysql database via connection pools.
