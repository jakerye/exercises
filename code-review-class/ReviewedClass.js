class T {
    constructor(width=null) {
        this._width = width;
    }

    getWidth() {  
        return this._width;
    }

    setWidth(width) {
        this._width = width;
    }
}

const t = new T();
t.setWidth(14);
console.log(t.getWidth())
