# Code Review: Simple Class
See the included InitialClass.js and ReviewedClass.js files.

## Review
This class is a great start but it could use a few enhancements. I see the intent behind the `width` method but unfortunately it is not returning the value of width but actually the `width` function. In general, it is a best practice to either access the public class variable directly or to use a getter method when computation needs to happen at runtime / if you anticipate requiring runtime computation in the future. In the case where you want to use a getter, it is also a best practice to name the method `getWidth` and to denote the class variable is private by prefixing the variable with an `_` (underscore), to communicate to other developers what is going on.

Another best practice is to use explicit declaration over implicit declarations. Right now, the class is implicitly setting the value of width to `undefined` which won’t cause the code to break, but is less clear to other developers what the initial value is. My personal preference is to initialize the value to `null` but it is best to follow the team / project conventions.
