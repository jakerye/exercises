# Code Review: Async Service
See the included initialCode.js and reviewedCode.js files.

## Review
This code is off to a good start but there is ambiguity as to what it is trying to accomplish. Is this code an endpoint that returns a response or is it middleware? I’d recommend taking a look at the express routing and middleware docs.

There is also an issue with how the function is declared. Using both the function preface and the arrow function `=>` to declare the function will cause an error. It is best to pick one or the other. My personal preference is to use the arrow function syntax because it is a bit more readable, however, it is best to follow the team / project conventions.

Assuming this code just needs to be a straightforward endpoint, it would be best to remove all the usages of `next()` and return the standard response object with standard HTTP conventions.

It is great to see you using the status code `200` for a successful response. However, seeing the status code in the response json does not follow standard conventions. It would be clearer to set the response status via the `res.status()` method.

Also, thinking about failure cases for functions is a great practice but unfortunately using try / catch blocks around an async method is not going to achieve the behavior you are looking for. Async methods always return a promise so would never throw an error. To catch errors from an async method you can append`.catch()` to the end of the function. However, since it looks like the async method accepts a callback function for errors, you shouldn’t need the extra error checking logic. You should double check the async function implementation to verify this.

Seeing you style your code to be `flat` is awesome! Checking for errors first then returning upon an error or continuing execution is a great practice that avoids complicated nesting logic that is easily prone to bugs. Just make sure that you are actually arereturning upon an error event. It is also good practice to be explicit about return values in general as it makes the code easier to understand and less prone to bugs.

As a final note, just be careful when trying to access object properties. In the case of checking for an error, if err is null and you try to access the status property that will break the code and end up returning a 500 response which is not ideal.
