const express = require('express');
const app = express();

function callAsyncMethod(data, callback) {
  error = null;
  return callback(error, { key: 'value' });
}

app.post('/data', (request, response) => {
  return callAsyncMethod(req.body, (error, result) => {
    if (error) {
      const { status, message } = error;
      return response.status(status).json({ message }).send();
    }
    return response.status(200).json(result).send();
  });
});

app.listen(3000)
