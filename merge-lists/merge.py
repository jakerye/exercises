from typing import List


def merge_lists(a: List, b: List, limit: int) -> List:
    print("Merging")

    # Check for valid limit
    if len(a) + len(b) < limit:
        raise Exception("Invalid limit")

    result = []
    count = 0

    while count < limit:

        # Get a value
        if len(a) > 0:
            a_value = a[0]
        else:
            a_value = None

        # Get b value
        if len(b) > 0:
            b_value = b[0]
        else:
            b_value = None

        # Check if a is empty
        if a_value == None:
            result.append(b[0])
            b = b[1:]

        # Check if b is empty
        elif b_value == None:
            result.append(a[0])
            a = a[1:]

        # Neither a or b are empty
        elif a_value < b_value:
            result.append(a[0])
            a = a[1:]
        else:
            result.append(b[0])
            b = b[1:]

        # Update count
        count += 1

    return result


if __name__ == "__main__":
    a = [-1, 1, 3, 4]
    b = [1, 5, 7]
    limit = 3

    result = merge_lists(a, b, limit)
    print(result)
