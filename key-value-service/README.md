# Key Value Service
Simple REST API for interacting with a key-value store with pluggable backends for an in memory store or sqlite store. Built with typescript, node, and express. Implements the Stratgy design pattern for pluggable backends and allows for the storage method to be updated from an endpoint.

## System Requirements
npm install -g typescript
npm install -g ts-node

## Usage
yarn install
yarn start

## Tests
curl -vv localhost:8000/store/a
curl -vv -d 123 localhost:8000/store/a
curl -vv localhost:8000/method
curl -vv -d memory localhost:8000/method
curl -vv -d sql localhost:8000/method

## References
- https://www.typescriptlang.org/
- https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html
- https://blog.sourcerer.io/a-crash-course-on-typescript-with-node-js-2c376285afe1
- https://blog.bitsrc.io/keep-it-simple-with-the-strategy-design-pattern-c36a14c985e9
- https://www.robertcooper.me/get-started-with-typescript-in-2019
