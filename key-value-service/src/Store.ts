export interface StorageMethod {
  name: string,
  getValue(key: string);
  setValue(key: string, value: string);
}

export class Store implements StorageMethod {

  public name;

  private storageMethod: StorageMethod

  constructor(storageMethod: StorageMethod) {
    this.storageMethod = storageMethod;
    this.name = storageMethod.name;
  }

  setMethod(storageMethod: StorageMethod) {
    this.storageMethod = storageMethod;
    this.name = storageMethod.name;
  }

  getValue(key: string) {
    return this.storageMethod.getValue(key);
  }

  setValue(key: string, value: string) {
    return this.storageMethod.setValue(key, value);
  }

}
