const sqlite = require('sqlite');
const SQL = require('sql-template-strings');

import { StorageMethod } from "./Store";

export default class StoreInSql implements StorageMethod {

  public name: string = 'sql';

  private databasePath = './mydatabase.sqlite';
  private database;

  constructor() {
    sqlite.open(this.databasePath).then((database) => {
      this.database = database;
      const sql = SQL`CREATE TABLE IF NOT EXISTS Store(key BLOB UNIQUE, value BLOB)`
      this.database.run(sql);
    });
  }

  async getValue(key: string) {
    console.log('Getting value from sqlite');
    const sql = SQL`SELECT value FROM Store WHERE key=${key}`;
    const response = await this.database.get(sql);
    return response ? response.value : undefined;
  }

  async setValue(key: string, value: string) {
    console.log('Setting value in sqlite');
    const sql = SQL`INSERT INTO Store (key, value) VALUES(${key}, ${value})
      ON CONFLICT(key) DO UPDATE SET value=${value}`
    await this.database.run(sql);
  }

}
