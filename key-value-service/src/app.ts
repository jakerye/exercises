import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Store } from "./Store";
import StoreInMemory from "./StoreInMemory";
import StoreInSql from "./StoreInSql";


class App {

  private store;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.store = new Store(new StoreInMemory())
  }

  public app: express.Application;

  private config(): void {
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes(): void {
    const router = express.Router();

    router.get('/store/:key', async (req: Request, res: Response) => {
      const { key } = req.params;
      const value = await this.store.getValue(key);
      console.log('Returning value:', value)
      if (!value) {
        return res.status(404).send()
      }
      res.send(value);
    });

    router.post('/store/:key', (req: Request, res: Response) => {
      const { key } = req.params;
      const value = Object.keys(req.body)[0]; // Hack
      this.store.setValue(key, value)
      res.status(204).send();
    });

    router.get('/method', (req: Request, res: Response) => {
      console.log('Getting storeage method: ', this.store.name);
      res.send(this.store.name);
    });

    router.post('/method', (req: Request, res: Response) => {
      const method = Object.keys(req.body)[0]; // Hack
      console.log("Setting storage method:", method);
      switch (method) {
        case "memory":
          this.store.setMethod(new StoreInMemory())
          return res.status(204).send()
        case "sql":
          this.store.setMethod(new StoreInSql())
          return res.status(204).send()
        default:
          return res.status(404).send()
      }
    });

    this.app.use('/', router)

  }

}

export default new App().app;
