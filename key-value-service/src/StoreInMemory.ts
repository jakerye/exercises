import { StorageMethod } from "./Store";

export default class StoreInMemory implements StorageMethod {

  public name: string = 'memory';

  private store = {}

  getValue(key: string) {
    console.log("Getting value from memory");
    return this.store[key];
  }

  setValue(key: string, value: string) {
    console.log("Setting value in memory");
    this.store[key] = value;
  }
}
