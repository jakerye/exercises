## Node Traversal
This can be done either recursively or with a stack. Both work fine but the recursive method is more prone to memory errors if the number of call stack nesting levels get sufficiently large. Also recursion can get confusing fairly quickly and introduce bugs so I tend to avoid it unless it is necessary.

The core idea behind the stack implementation is to travel to the left most node whenever possible and push the nodes to a stack. After hitting the left most node, that node gets popped off the stack and the traversal moves to the next right node.

See `node_traversal.py`.

## Node Distance
To calculate the distance between two nodes, we need to find the lowest common ancestor node, calculate the depth from each node to the ancestor node, then sum the depths. 

I chose to do this by extending the traversal function to generate a list of the parents for each node then compared the parent lists to find the ancestor node and counted the steps between them.

See `node_distance.py`.