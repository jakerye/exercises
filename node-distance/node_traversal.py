class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def traverse(root):

    # Initialize parameters
    current = root
    stack = []

    # Traverse the tree
    while True:

        # Travel to left-most node
        if current is not None:
            stack.append(current)
            current = current.left

        # Select node then move to next right node
        elif stack:
            selected = stack.pop()
            print(selected, end=" ")
            current = selected.right

        # Reached the end of the tree
        else:
            break


if __name__ == "__main__":

    # Level 0
    root = Node('a')

    # Level 1
    root.left = Node('b')
    root.right = Node('c')

    # Level 2
    root.left.left = Node('d')
    root.left.right = Node('e')
    root.right.left = Node('f')
    root.right.right = Node('g')

    # Level 3 (Left)
    root.left.left.left = Node('h')
    root.left.left.right = Node('i')
    root.left.right.left = Node('j')
    root.left.right.right = Node('k')

    # Level 3 (Right)
    root.right.left.left = Node('l')
    root.right.left.right = Node('m')
    root.right.right.left = Node('n')
    root.right.right.right = Node('o')

    # Traverse the tree
    traverse(root)  # h d i b j e k a l f m c n g o
