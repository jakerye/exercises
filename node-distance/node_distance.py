class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.data)


def get_parent_nodes(root, node):

    # Initialize parameters
    current = root
    stack = []
    parents = []

    # Traverse the tree
    while True:

        # Travel to left-most node
        if current is not None:
            stack.append(current)
            parents.append(current.data)
            current = current.left

        # Select node then move to next right node
        elif stack:
            selected = stack.pop()
            current = selected.right

            # Update parents
            index = parents.index(selected.data)
            parents = parents[: index + 1]

            # Check for node match
            if selected.data == node:
                return parents[:-1]

        # Reached the end of the tree
        else:
            break

    # Node not in tree
    print("Node is not in the tree")
    return []


def calculate_distance(root, node1, node2):

    # Get node parents
    parents1 = get_parent_nodes(root, node1)
    parents2 = get_parent_nodes(root, node2)

    # Reverse the lists
    parents1.reverse()
    parents2.reverse()

    # Check if node 1 is a parent of node 2
    if node1 in parents2:
        ancestor = node1
        distance = parents2.index(node1) + 1
        return ancestor, distance

    # Check if node 2 is a parent of node 1
    if node2 in parents1:
        ancestor = node2
        distance = parents1.index(node2) + 1
        return ancestor, distance

    # Find least common ancestor node
    for node in parents1:
        if node in parents2:
            ancestor = node
            node_1_index = parents1.index(node)
            node_2_index = parents2.index(node)
            distance = node_1_index + node_2_index + 2
            return ancestor, distance

    # No common ancestor node
    return None, None


if __name__ == "__main__":

    # Level 0
    root = Node("a")

    # Level 1
    root.left = Node("b")
    root.right = Node("c")
    root.left.left = Node("d")
    root.left.right = Node("e")
    root.right.left = Node("f")
    root.right.right = Node("g")
    root.left.left.left = Node("h")
    root.left.left.right = Node("i")
    root.left.right.left = Node("j")
    root.left.right.right = Node("k")
    root.right.left.left = Node("l")
    root.right.left.right = Node("m")
    root.right.right.left = Node("n")
    root.right.right.right = Node("o")

    # Calculate distance
    ancestor, distance = calculate_distance(root, "h", "o")  # a, 6
    ancestor, distance = calculate_distance(root, "h", "k")  # b, 4
    ancestor, distance = calculate_distance(root, "e", "k")  # e, 1
    ancestor, distance = calculate_distance(root, "k", "a")  # a, 3
    print(ancestor, distance)
