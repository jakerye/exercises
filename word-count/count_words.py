import re, json
from typing import Dict

# Get words
with open('constitution.txt', 'r') as text_file:
    text_string = text_file.read().lower()
    words = re.findall(r'[a-z]+', text_string)

# Get word count
word_count: Dict[str, int] = {}
for word in words:
    count = word_count.get(word, 0)
    word_count[word] = count + 1

# Sort words by ascending frequency then alphabetically
sorted_words = sorted(word_count.items(), key=lambda x: (x[1], x[0]))

# Write words to file
with open('words.txt', 'w') as text_file:
    [text_file.write(f"{w[0]}: {w[1]}\n") for w in sorted_words]
