const axios = require('axios')

const devices = ["0x00077C98", "0x00078D38", "0x0007964F"]
const deviceDataArr = []

async function fetchSequential() {
    for (const [index, device] of devices.entries()) {
        console.log(`Getting ${device}`)
        await axios.get(`http://www.mocky.io/v2/5deeab8933000029bd9848d0?` + device)
            .then(response => {
                console.log(`Got ${device}`)
                deviceDataArr[index] = response.data;
            }).catch(err => console.log(err))
    }
}

fetchSequential()
