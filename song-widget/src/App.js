import React from 'react';
import './App.css';
import CurrentSongWidget from './components/CurrentSongWidget';

const onlyInDreams = {
    title: 'Only in Dreams',
    artist: 'Weezer',
    albumImage: 'https://bit.ly/33YiuWF',
}

const longSongTitle = {
    title: 'Only in Dreams can You Count Sheep',
    artist: 'Weezer',
    albumImage: 'https://bit.ly/33YiuWF',
}

function App() {
    return (
        <div className="App" style={{ margin: 20 }}>
            <CurrentSongWidget
                currentSong={longSongTitle}
                onPause={() => console.log('Paused')}
                onPlay={() => console.log('Playing')}
            />
        </div>
    );
}

export default App;
