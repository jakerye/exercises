import React, { useState, useEffect } from 'react';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons'


function getTextWidth(text, font) {
  const element = document.createElement('canvas');
  const context = element.getContext('2d');
  context.font = font;
  return context.measureText(text).width;
}

export default function CurrentSongWidget(props) {

  // Initialize state
  const [isPlaying, setIsPlaying] = useState(false);
  const [previousSong, setPreviousSong] = useState(null);

  // Initialize props
  const { currentSong } = props;
  let { height, width } = props;

  // Handle component updates
  useEffect(() => {
    if (previousSong !== currentSong) {
      setIsPlaying(true);
      setPreviousSong(currentSong);
    }
  });

  // Initialize styles
  const minHeight = 50;
  const defaultHeight = 60;
  const minWidth = 250;
  const defaultWidth = 350;
  const font = '16px arial'

  // Validate height prop
  if (!height) {
    height = defaultHeight;
  } else if (height < minHeight) {
    console.error(`Component height must be at least ${minHeight} px`);
    height = minHeight;
  }

  // Validate width prop
  if (!width) {
    width = defaultWidth;
  } else if (width < minWidth) {
    console.error(`Component width must be at least ${minWidth} px`);
    width = minWidth;
  }

  // Get text widths
  const titleWidth = getTextWidth(currentSong.title, font);
  const artistWidth = getTextWidth(currentSong.artist, font);
  const textBoxWidth = width - height * 2;

  // Determine marquee state
  const titleIsMarqueed = textBoxWidth - 10 < titleWidth;
  const artistIsMarqueed = textBoxWidth - 10 < artistWidth;

  // Handle when play button is pushed
  const onPlay = () => {
    setIsPlaying(true);
    props.onPlay && props.onPlay();
  };

  // Handle when pause button is pushed
  const onPause = () => {
    setIsPlaying(false);
    props.onPause && props.onPause();
  }

  // Initialize styles
  const styles = {
    card: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: 'lightgrey',
      width: width,
      height: height,
    },
    image: {
      height: height,
      width: height,
    },
    button: {
      height: height,
      width: height,
      borderRadius: 0,
    },
    text: {
      font,
    }
  }

  // Render component
  return (
    <div style={styles.card}>
      <img
        src={currentSong.albumImage}
        style={styles.image}
      />
      <div style={styles.text}>
        {titleIsMarqueed ? <marquee>{currentSong.title}</marquee>
          : <span>{currentSong.title}</span>} <br />
        {artistIsMarqueed ? <marquee>{currentSong.artist}</marquee>
          : <span>{currentSong.artist}</span>}
      </div>

      {!isPlaying && (
        <Button onClick={onPlay} style={styles.button}>
          <FontAwesomeIcon icon={faPlay} />
        </Button>
      )}
      {isPlaying && (
        <Button onClick={onPause} style={styles.button}>
          <FontAwesomeIcon icon={faPause} />
        </Button>
      )}
    </div>
  )
}
