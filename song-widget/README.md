# Song Widget

The following component accepts a song object, pause/play callbacks, and a few style props. It checks to see if the song title and artist fit within the text box and if they do not it marquees the text. Also, the pause/play callbacks get called after the song widget has been paused or resumed. Upon loading a new song, the widget assumes it is currently playing. If this assumption is no good then the component could also easily be modified to get the current play state via props.

## Usage
```
yarn install
yarn start
```