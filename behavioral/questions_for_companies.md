# Questions to Ask Companies
Starter questions to ask companies. The more you interview them the more information you get, the more interested in the position you will seem, and in general will lead to better discussions. These questions should serve as an entry point for starting the conversation then evolve into more specific and more highly related questions as you get to know the company better. Don't be afraid to question fundamental parts of the business or technology choices. Senior people tend to respect when you kick the tires of their business and technical decisions. It is also a chance to show how much you know about certain fields and how much you have learned throughout the interview process.

## Business
1. What problem do you solve?
2. How many people are in the company?
3. What funding stage are you at?
4. How long is your runway?

## Role
1. How big is the sub-team?
2. What does day-to-day look like for this role?
3. What is a representative project I’d be working on?

## Leadership
1. What is your background?
2. Why did you join the company?
3. How do you spend your time?
4. What do you think the company needs to improve?

## Technology
1. What is your high level architecture?
2. How do you ensure code quality?

## Roadmap
1. What is your team hoping to achieve in the next 3 months?
2. Where is the company heading in the next 5 years?
3. What do you think the company needs to absolutely get right to be wildly successful?

## Logistics
1. Where are you located?
2. What are your core hours?
3. Do you have a policy for remote work?
4. What is your runway?
5. What is your interview process?

## Other Ideas
1. https://github.com/viraptor/reverse-interview
