# Mock Interview Questions
Read each question, prepare an answer mentally, then read what the question was "testing" for and evolve your answer.

## Collections
1. https://firstround.com/review/40-favorite-interview-questions-from-some-of-the-sharpest-folks-we-know/
2. https://www.businessinsider.com/executives-favorite-job-interview-question-2014-11
3. https://www.inc.com/jeff-haden/27-most-common-job-interview-questions-and-answers.html
