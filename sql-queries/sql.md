# SQL Queries

See: https://data36.com/sql-interview-questions-tech-screening-data-analysts/

## Top 3 Best-Selling Authors
SELECT authors.author_name, SUM(books.sold_copies) AS sold_sum
FROM authors 
JOIN books
ON authors.book_name = books.book_name
GROUP BY authors.author_name 
ORDER BY sold_sum DESC
LIMIT 3;

## Users w/1000-2000 Images
SELECT COUNT(*) FROM
(SELECT user_id, COUNT(event_date_time) AS num_images
FROM event_log
GROUP BY user_id) WHERE num_images > 1000 AND num_images < 2000;

## Departments w/Average Salaries Below $500
SELECT employees.department_name, AVG(salaries.salary) AS avg_salary
FROM employees JOIN salaries ON employees.employee_id = salaries.employee_id
GROUP BY employees.department_name
HAVING avg_salary < 500;
