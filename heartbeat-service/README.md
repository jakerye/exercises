# Heartbeat Service

This code demonstrates a heartbeat service that clients can check into via TCP sockets to update their connection status to be ONLINE, LIMBO, or OFFLINE.
