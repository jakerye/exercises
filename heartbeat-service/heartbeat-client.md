# Heartbeat client
A simple NodeJs based TCP client that connects to the Heartbeat server.

# Requirements
Node 8+

# Defaults
IP: 127.0.0.1
PORT: 7777
PASSPHRASE: pass123
HEARTBEAT_INTERVAL: 10 secs

# Run instructions

Run using default IP and port
`node heartbeat-client.js`

Customize IP and port
`IP=192.168.1.2 PORT=8888 node heartbeat-client.js`

# Client commands
Once running, you can issue the following commands:

### Send raw message
To send a raw message to the server:
`send <message>`

### Login
To login with default passphrase:
`login alice`

To login with custom passphrase:
`login alice my_pass_phrase`

### Heartbeat
To perform a single heartbeat:
`heartbeat`

To start heartbeating (every 10 secs): 
`heartbeat start`

To stop heartbeating:
`heartbeat stop`

### Reconnect to server
`reconnect`

### Quit
`exit` or just CTRL+C if you think life is too short