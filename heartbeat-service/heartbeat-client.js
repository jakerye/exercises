var net = require('net');
const readline = require('readline');

const DEFAULT_PASSPHRASE = 'pass123';
const DEFAULT_HEARTBEAT_INTERVAL = 5000;
const DEFAULT_PORT = 7777;
const DEFAULT_IP = '127.0.0.1';
let loginToken;
let hearbeatTimer;
let client;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.on('line', (input) => {
  const args = input.trim().split(' ');
  if (args.length === 0) {
    return;
  }

  switch (args[0]) {
    case 'login':
      doLogin(args[1], args[2]);
      break;
    case 'heartbeat':
      switch (args[1]) {
        case 'start':
          startHeartbeat(args[2]);
          break;
        case 'stop':
          stopHeartbeat(args[2]);
          break;
        default:
          doHeartbeat(args[1]);
          break;
      }
      break;
    case 'send':
      if (args[1] != null && args[1].length > 0) {
        const [cmd, ...msg] = args;
        send(msg.join(' '));
      } else {
        console.log('Specify a message like `send hello`')
      }
      break;
    case 'exit':
      exit();
      break;
    case 'reconnect':
      connect();
      break;
    default:
      console.log('Unknown command!');
      console.log(`
        Supported Commands:
          login <client_id> [<passphrase>]
          heartbeat [start | stop]
          send <message>
          exit
      `)
  }

});


// HELPERS

function connect() {
  if (client != null) {
    client.destroy();
  }

  client = new net.Socket();
  client.connect(process.env.PORT || DEFAULT_PORT, process.env.IP || DEFAULT_IP, function () {
    console.log('Connected to server');
  });

  client.on('data', function (data) {
    console.log('[FROM SERVER]: ' + data);
    const frame = data.toString('utf-8').split('|');
    switch (frame[0]) {
      case 'login_ok':
        const token = frame[1];
        if (token != null && token.length > 0) {
          loginToken = token;
          console.log('Login successful. Token is: ', loginToken);
          return;
        }
      case 'pong':
        console.log('Got Pong');
        return;
      case 'error':
        console.log(`ERROR: ${frame[2]} [Code: ${frame[1]}]`);
        if (frame[1] === '401') {
          stopHeartbeat();
          loginToken = null;
        }
        return;
      default:
        console.log('uhh? whaat??');
    }
  });

  client.on('close', function () {
    console.log('Connection closed');
    client.destroy();
  });

  client.on('error', function (err) {
    console.error('Error: ', err);
    client.destroy();
  });

  client.on('timeout', function () {
    console.error('Timeouts');
    client.destroy();
  });
}

function doLogin(clientId, passphrase = DEFAULT_PASSPHRASE) {
  console.log('Attempting to login...');
  send(`login|${clientId}|${passphrase}`);
}

function doHeartbeat(token = loginToken) {
  if (token == null) {
    console.log('No token available for heartbeat');
    return;
  }

  send(`ping|${token || loginToken}`);
}

function startHeartbeat(interval) {
  stopHeartbeat()

  const customInterval = parseInt(interval);
  let heartbeatInterval = isNaN(customInterval) ? DEFAULT_HEARTBEAT_INTERVAL : customInterval;
  doHeartbeat();
  hearbeatTimer = setInterval(doHeartbeat, heartbeatInterval);
  console.log('Started heartbeating...');
}

function stopHeartbeat() {
  if (hearbeatTimer != null) {
    clearInterval(hearbeatTimer);
    console.log('Stopped heartbeating...');
  }
}

function exit() {
  console.log('Bye bye');
  client.destroy();
  process.exit(0);
}

function send(frame) {
  console.log('Sending frame: ', frame);
  client.write(frame);
}

connect();
