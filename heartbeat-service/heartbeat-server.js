const net = require('net');
const port = 7777;
const host = '127.0.0.1';

const server = net.createServer();
server.listen(port, host, () => {
  console.log('TCP Server is running on port ' + port + '.');
});

const clients = {};
let tokenCounter = 1;

server.on('connection', function (sock) {
  console.log('CONNECTED: ' + sock.remoteAddress + ':' + sock.remotePort);

  sock.on('data', function (data) {
    const commands = data.toString().split('|');
    console.log('commands:', commands);
    const commandType = commands[0];
    if (commandType === 'login') {
      const userName = commands[1];
      const password = commands[2];
      if (password !== 'pass123') {
        console.log('Invalid password');
        const response = 'error|401|unauthorized';
        return sock.write(response);
      }
      const token = `fake_token_${tokenCounter}`;
      tokenCounter += 1;
      console.log('Authenticated!');
      const lastUpdated = Date.now();
      const connectionState = 'ONLINE';
      clients[token] = { userName, lastUpdated, connectionState };
      const response = `login_ok|${token}`;
      return sock.write(response);
    } else if (commandType === 'ping') {
      console.log('Received ping!')
      const token = commands[1];
      if (!Object.keys(clients).includes(token)) {
        console.log('Invalid user');
        const response = 'error|401|unauthorized';
        return sock.write(response);
      }
      console.log('Valid user');
      clients[token].lastUpdated = Date.now();
      clients[token].connectionState = 'ONLINE';
      const response = 'pong';
      return sock.write(response);
    }
  });
});

// Monitoring
function updateMonitor() {
  console.log('Updating monitor');

  Object.keys(clients).forEach((token) => {
    clientData = clients[token];
    const { userName, lastUpdated, connectionState } = clientData;
    const currentTime = Date.now();
    const updateDelta = currentTime - lastUpdated;
    if (connectionState === 'ONLINE' && updateDelta > 6000) {
      console.log(`${userName}: Entered LIMBO`);
      clientData.connectionState = 'LIMBO';
    } else if (connectionState === 'LIMBO' && updateDelta > 12000) {
      console.log(`${userName}: Entered OFFLINE`);
      clientData.connectionState = 'OFFLINE';
      delete clients[token];
    } else if (connectionState === 'LIMBO' && updateDelta < 6000) {
      console.log(`${userName}: Entered ONLINE`);
      clientData.connectionState = 'ONLINE';
    }
  });

  console.log('Clients:', clients);
  setTimeout(updateMonitor, 5000);
}

updateMonitor();
